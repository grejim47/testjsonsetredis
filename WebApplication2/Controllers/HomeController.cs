﻿using MessagePack;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NReJSON;
using ServiceStack;
using ServiceStack.Redis;
using StackExchange.Redis;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text.Json.Nodes;
using WebApplication2.Models;
using static System.Net.Mime.MediaTypeNames;

namespace WebApplication2.Controllers
{
    [MessagePackObject]
    public class Test
    {
        [Key("Nombre")]
        public string Nombre { get; set;}
        [Key("Email")]
        public string Email { get; set;}
        [Key("Direccion")]
        public string Direccion { get; set;}

    }


    public class Rootobject
    {
        public List<ActiveChatRedis> Data { get; set; }
    }



    public class ActiveChatRedis
    {
        public string User { get; set; } = "user";
        public DateTime Date { get; set; } = DateTime.Now;
        public string Room { get; set; } = "room";
        public string Queue { get; set; } = "queue";
        public string UserConnected { get; set; } = "on";
        public string Region { get; set; } = "Region";
        public string Agent { get; set; } = "Agent";
    }
    public static class Utils
    {
        public static string ToJson(this object obj) => JsonConvert.SerializeObject(obj);
    }
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IConnectionMultiplexer _redis;
        private IDictionary<string, string> _dictionary;
        private readonly RedisEndpoint _endpoint = new RedisEndpoint("localhost", 6379);

        public HomeController(ILogger<HomeController> logger, IConnectionMultiplexer redisssss)
        {
            _logger = logger;
            _redis = redisssss;
        }

        public IActionResult Index()
        {
            var db = _redis.GetDatabase();


        Extender:

            List<ActiveChatRedis> asd = new List<ActiveChatRedis>();
            ActiveChatRedis nuevo = new ActiveChatRedis();
            asd.Add(nuevo);
            Rootobject data = new Rootobject();
            data.Data = asd;
            db.JsonSet("MiJsonArray", asd.ToJson());



            //var list = db.ListRange("ActiveChatsRedis");
            //example
            //var hola = db.JsonGet("MiJsonArray", new string[] { "$.[?(@.User=='user2' && @.Room=='room')]" });
            #region AÑADIR A LISTA
            var lista = db.KeyExists("MiJsonArray");
            if (!lista)
                db.JsonSet("MiJsonArray", "[]");
            db.JsonArrayAppend("MiJsonArray", ".", asd.ToJson());//Agregar a lista
            #endregion
            JArray nuevo2 = JArray.Parse(db.JsonGet("MiJsonArray", "$.*").ToString());
            
            #region CONVERTIR A OBJETO
            try
            {
                JsonConvert.DeserializeObject<List<ActiveChatRedis>>(lista.ToString());
            }
            catch (Exception)
            {

            }
            #endregion

            #region DESENCOLAR PRIMERA POSICION
            var reg = db.JsonArrayPop("MiJsonArray", index:0);//Desencolar
            #endregion
            goto Extender;

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

    }
    public static class redisUtils
    {
        public static IDictionary<string, string> GetRedis(this IDictionary<string, string> dictionary, IConnectionMultiplexer connectionMultiplexer)
        {
            var db = connectionMultiplexer.GetDatabase();
            var rRes = db.ListRange("HolaMundo");
            var hola = rRes.JsonToObject<Dictionary<string, string>>();
            return null;
        }

        public static T JsonToObject<T>(this RedisValue[] obj)
        {
            var json = JsonConvert.SerializeObject(obj);
            return JsonConvert.DeserializeObject<T>(json);
        }
        public static RedisClient GetDataBase(this RedisEndpoint obj)
        {
            return new RedisClient(obj);
        }
    }
}